package com.soflete.lib;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.soflete.lib.ScreenGraph.Node;

public abstract class BaseScreen implements Screen {

	protected final Game mGame;
	private String mTag;
	private Node mParentNode;

	public BaseScreen(Game game, String tag) {
		mGame = game;
		mTag = tag;
	}

	public void create() {
		onCreate();
	}

	public BaseScreen findScreen(String tag) {

		if (mTag.equals(tag)) {
			return this;
		}

		final BaseScreen screen = mParentNode.find(tag);
		if (screen == null) {
			throw new IllegalArgumentException(
					"could not find a GolfScreen of the specified screenType: "
							+ tag);
		}

		return screen;
	}

	public ScreenGraph.Node getParentNode() {
		return mParentNode;
	}

	public String getTag() {
		return mTag;
	}

	@Override
	public void hide() {
	}

	protected abstract void onCreate();

	protected void onResume() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public final void resume() {
		Assets.getInstance().finishLoading();
		onResume();
	}

	public void setParentNode(Node mParentNode) {
		this.mParentNode = mParentNode;
	}

	@Override
	public void show() {
	}

	public void setScreen(BaseScreen screen) {

		if (!getParentNode().equals(screen.getParentNode())) {
			screen.getParentNode().createScreens();

			Assets.getInstance().finishLoading();
		}

		mGame.setScreen(screen);
	}

}
