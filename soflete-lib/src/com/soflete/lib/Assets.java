package com.soflete.lib;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

public class Assets extends AssetManager {

	private static Assets sInstance;

	public static Assets getInstance() {
		if (sInstance == null) {
			sInstance = new Assets();
			Texture.setAssetManager(sInstance);
		}
		return sInstance;
	}

	private Assets() {
	}

}
