package com.soflete.lib;

import java.util.HashSet;
import java.util.Set;

public class ScreenGraph {

	public static final class Node {

		private HashSet<BaseScreen> mScreens;
		private ScreenGraph mGraph;

		public Node(ScreenGraph screenGraph) {
			mGraph = screenGraph;
			mScreens = new HashSet<BaseScreen>();
		}

		public boolean add(BaseScreen screen) {

			if (screen.getParentNode() != null) {
				// Already a part of the graph
				return false;
			}

			screen.setParentNode(this);

			return mScreens.add(screen);
		}

		public void createScreens() {
			for (BaseScreen screen : mScreens) {
				screen.create();
			}
		}

		public BaseScreen find(String tag) {

			for (BaseScreen screen : mScreens) {
				if (screen.getTag().equals(tag)) {
					return screen;
				}
			}

			for (Node node : mGraph.mNodes) {
				if (!equals(node)) {
					for (BaseScreen screen : node.mScreens) {
						if (screen.getTag().equals(tag)) {
							return screen;
						}
					}
				}
			}

			return mGraph.find(tag);
		}

	}

	private final Set<Node> mNodes;

	private HashSet<BaseScreen> mScreens;

	public ScreenGraph() {
		mNodes = new HashSet<ScreenGraph.Node>();
		mScreens = new HashSet<BaseScreen>();
	}

	public void add(BaseScreen screen) {
		mScreens.add(screen);
	}

	public Node createNode() {
		final Node node = new Node(this);
		mNodes.add(node);
		return node;
	}

	public BaseScreen find(String tag) {

		for (BaseScreen screen : mScreens) {
			if (screen.getTag().equals(tag)) {
				return screen;
			}
		}

		return null;
	}
}
