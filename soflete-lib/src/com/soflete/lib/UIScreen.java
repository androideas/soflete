package com.soflete.lib;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class UIScreen extends BaseScreen {

	protected static final float LAYOUT_WIDTH = 480.0f;
	protected static final float LAYOUT_HEIGHT = 800.0f;

	private Actor mLayout;
	private boolean mLayoutCreated;
	private Stage mStage;
	private OrthographicCamera mStageCamera;
	private InputMultiplexer mInputMultiplexer;

	public UIScreen(Game game, String tag) {
		super(game, tag);

		mStage = new Stage();
		mStageCamera = new OrthographicCamera();

		mStage.setCamera(mStageCamera);

		mInputMultiplexer = new InputMultiplexer();
		mInputMultiplexer.addProcessor(mStage);
	}

	@Override
	public void dispose() {
		mLayoutCreated = false;
	}

	@Override
	public void hide() {
		onHide();
	}

	private boolean isLayoutCreated() {
		return mLayoutCreated;
	}

	protected abstract Actor onCreateLayout();

	protected abstract void onHide();

	protected void onRender(float delta) {
	}

	protected abstract void onShow();

	@Override
	public final void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		mStage.act(delta);
		mStage.draw();

		onRender(delta);
	}

	@Override
	public void resize(int width, int height) {
		mStage.setViewport(LAYOUT_WIDTH, LAYOUT_HEIGHT, true);
		mStageCamera.position.set(LAYOUT_WIDTH / 2, LAYOUT_HEIGHT / 2, 0);
		mStageCamera.update();
	}

	@Override
	public final void show() {

		if (!isLayoutCreated()) {
			mLayout = onCreateLayout();
			if (mLayout != null) {
				mStage.clear();
				mStage.addActor(mLayout);
			}
			mLayoutCreated = true;
		}

		onShow();

		Gdx.input.setInputProcessor(mInputMultiplexer);
	}
}
